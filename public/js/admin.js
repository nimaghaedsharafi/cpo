(function() {
	$('#menu *').disableSelection();
)();

$('.nav li').mouseover(function(){
	$(this).find('a i').addClass('icon-2x');
}).on("mouseleave",function(){
	$(this).find('a i').removeClass('icon-2x');
});
$('.nav-header').click(function() {
	$(this).nextUntil('li.divider').toggle('hide').delay(50);
	var item = $(this).find('i');
	if(item.attr('class') == 'icon-chevron-left')
		item.attr('class' , 'icon-chevron-down');
	else
		item.attr('class' , 'icon-chevron-left');

});
$('#newunit').click(function(e){
	e.preventDefault();
	console.log($('#newunitmodal').length );
	if($('#newunitmodal').length  == 0)
	{
		$.ajax({
			'type': 'get',
			'dataType': 'html',
			'url' : '{{ URL::to_route('newunit') }}',
			'success' : function(r) {
				$('body').append(r);
				$('#newunitmodal').modal();
			}
		});
	}
	else
	{
		$('#newunitmodal').modal();
	}
});