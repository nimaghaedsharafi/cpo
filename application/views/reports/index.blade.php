@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')
    <h2> {{ $title }} </h2>
	<?php if(Session::has('msg')) { echo Misc::alert(Session::get('msg'), Session::get('state') ); } ?>

@foreach($reports->results as $report)
<div>
	<div class="span8 well">
        <h2>
        	{{ $report->subject }}
        	@if(Auth::User()->id == $report->uid)
        		<a href="{{ URL::to_route('deletereport', $report->id) }}" class="btn pull-left" onclick="return confirm('آیا از حذف این گزارش اطمینان دارید؟')"><i class="icon-trash"></i></a>
        		<a href="{{ URL::to_route('editreport', $report->id) }}" class="btn pull-left"><i class="icon-edit"></i></a>
        	@endif
        </h2>
        <h4><small>توسط: {{ $report->author->name }} در تاریخ {{ Misc::niceDateForge($report->created_at, 'j F y') }}
        <small></h4>
        <p>{{ Misc::cutWord($report->content, 230)}}</p>
        <h6><a class="pull-left btn" href="{{ URL::to_route('showreport', $report->id) }}">ادامه گزارش... </a></h6>
    </div>
</div>
@endforeach
<div class="pagination pagination-centered">
	{{ $reports->links() }}
</div>
@endsection