<?php $user = Auth::User(); ?>
<div class="span3">
    <div class="well sidebar-nav">
        <ul id="menu" class="nav nav-list"> 
            <li class="nav-header"><i class="icon-chevron-down"></i> منو اصلی</li>
            <li class="sub" ><a href="{{ URL::to_route('home') }}"><i class="icon-home"></i> صفحه اول</a></li>
            <li class="sub" ><a href="{{ URL::to_route('home') }}"><i class="icon-key"></i> تغییر رمز</a></li>
            <li class="sub" ><a href="{{ URL::to_route('contactus') }}"><i class="icon-comment"></i> ارتباط با ما</a></li>
            <li class="sub" ><a href="{{ URL::to_route('aboutus') }}"><i class="icon-group"></i> درباره ی ما</a></li>
            <li class="sub" ><a href="{{ URL::to_route('logout') }}"><i class="icon-off"></i> خروج</a></li>
            
            
            <li class="divider"></li>

            <li class="nav-header"><i class="icon-chevron-down"></i> خاطرات</li>
            <li class="sub" ><a href="{{ URL::to_route('memoriesindex') }}"><i class="icon-archive"></i> خاطره ها</a></li>
            <li class="sub" ><a href="{{ URL::to_route('newmemory') }}"><i class="icon-pencil"></i> نوشتن خاطره</a></li>
            @if(!is_null($user) && $user->access == ADMIN_ACCESS)
            <li class="sub" ><a href="{{ URL::to_route('commentsindex') }}"><i class="icon-check"></i> تائید نظرات</a></li>
            @endif

            <li class="divider"></li>

            <li class="nav-header"><i class="icon-chevron-down"></i> گزارش ها</li>
            <li class="sub" ><a href="{{ URL::to_route('reportsindex') }}"><i class="icon-archive"></i> گزارشات</a></li>
            <li class="sub" ><a href="{{ URL::to_route('newreport') }}"><i class="icon-pencil"></i> نوشتن گزارش</a></li>

            <li class="divider"></li>

            <li class="nav-header"><i class="icon-chevron-down"></i> مقاله ها</li>
            <li class="sub" ><a href="{{ URL::to_route('articleindex') }}"><i class="icon-archive"></i> مقالات</a></li>
            <li class="sub" ><a href="{{ URL::to_route('newarticle') }}"><i class="icon-pencil"></i> نوشتن مقاله</a></li>

            <li class="divider"></li>

            <li class="nav-header"><i class="icon-chevron-down"></i> خبر ها</li>
            <li class="sub" ><a href="{{ URL::to_route('newsindex') }}"><i class="icon-archive"></i> اخبار</a></li>
            <li class="sub" ><a href="{{ URL::to_route('newnews') }}"><i class="icon-pencil"></i> نوشتن خبر</a></li>
            @if(!is_null($user) && $user->access == ADMIN_ACCESS)
            <li class="sub" ><a href="{{ URL::to_route('newscommentsindex') }}"><i class="icon-check"></i> تائید نظرات</a></li>
            @endif

            @if(!is_null($user) && $user->access == ADMIN_ACCESS)

            <li class="divider"></li>

            <li class="nav-header"><i class="icon-chevron-down"></i> تنظیمات</li>
            <li class="sub" ><a href="{{ URL::to_route('settings') }}"><i class="icon-gear"></i> تنظیمات</a></li>
            <li class="sub" ><a href="{{ URL::to_route('usersindex') }}"><i class="icon-group"></i> مدیریت کاربران</a></li>
            @endif
        </ul>
    </div><!--/.well -->
</div><!--/span-->