@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')

{{ Form::open(URL::to_route('newmemory')) }}

    <h2> {{ $title }}  {{ Form::submit('ثبت', ['class' => 'btn btn-warning']) }}</h2>

	{{ Form::text('subject', '', ['class' => 'span4', 'placeholder' => 'عنوان خاطره']) }}
	<div>
		{{ Form::textarea('mem', '', ['class' => 'span6 makeTextNice', 'placeholder' => 'متن خاطره ...']) }}
	</div>
	
{{ Form::close() }}

@endsection
