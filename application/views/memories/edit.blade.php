@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')

{{ Form::open(URL::to_route('updatememory')) }}
{{ Form::hidden('mid', $mem->id) }}
    <h2> {{ $title }}  {{ Form::submit('ثبت', ['class' => 'btn btn-warning']) }}</h2>

	{{ Form::text('subject', $mem->subject, ['class' => 'span4', 'placeholder' => 'عنوان خاطره']) }}
	<div>
		{{ Form::textarea('mem', $mem->mem, ['class' => 'span6 makeTextNice', 'placeholder' => 'عنوان خاطره']) }}
	</div>
	
{{ Form::close() }}

@endsection
