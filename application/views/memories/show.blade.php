@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')
    <h2> {{ $mem->subject }} </h2>
	<?php if(Session::has('msg')) { echo Misc::alert(Session::get('msg'), Session::get('state') ); } ?>
    
<div>
	<h4><small>توسط: {{ $mem->author->name }} در تاریخ {{ Misc::niceDateForge($mem->created_at, 'j F y') }}</small></h4>
	<p>{{ $mem->mem }}</p>
</div>
<div>
<?php $hasAdminAccess = Auth::User()->access == ADMIN_ACCESS; ?>
@foreach($mem->comments as $comment)
<div class="well">
	<h3>
		{{ $comment->name }} - <small>{{ Misc::niceDateForge($comment->created_at) }}</small>
		@if($hasAdminAccess and $comment->accepted == true)
    		<a href="{{ URL::to_route('verifycm', [$comment->id]) }}" class="btn pull-left"><i class="icon-eye-close"></i></a>
    	@elseif($hasAdminAccess and $comment->accepted == false)
	    	<a href="{{ URL::to_route('verifycm', [$comment->id, !$comment->accepted]) }}" class="btn pull-left"><i class="icon-eye-open"></i></a>
	    @endif
	</h3>
	{{ $comment->comment }}
</div>
@endforeach
</div>
<div>
<h3>ارسال نظر</h3>
{{ Form::open(URL::to_route('leavecomment')) }}
{{ Form::hidden('mid', $mem->id) }}

{{ Form::text('name', '', ['class' => 'span3', 'placeholder' => 'نام شما']) }}
<div>{{ Form::textarea('comment', '', ['class' => 'span4 makeTextNice' , 'placeholder' => 'متن نظر خود را بنویسید....']) }}</div>

{{ Form::submit('ارسال نظر', ['class' => 'btn']) }}

{{ Form::close() }}
</div>

@endsection