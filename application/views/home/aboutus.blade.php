@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')
    <h2> {{ $title }} </h2>

<div class="well">{{ $content }}</div>

@endsection
