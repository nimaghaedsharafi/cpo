@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')

{{ Form::open(URL::to_route('newnews')) }}

    <h2> {{ $title }}  {{ Form::submit('ثبت', ['class' => 'btn btn-warning']) }}</h2>

	{{ Form::text('subject', '', ['class' => 'span4', 'placeholder' => 'عنوان خبر']) }}
	<div>
		{{ Form::textarea('content', '', ['class' => 'span6 makeTextNice', 'placeholder' => 'متن خبر ...']) }}
	</div>
	
{{ Form::close() }}

@endsection
