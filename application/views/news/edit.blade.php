@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')

{{ Form::open(URL::to_route('updatenews')) }}
{{ Form::hidden('nid', $news->id) }}
    <h2> {{ $title }}  {{ Form::submit('ثبت', ['class' => 'btn btn-warning']) }}</h2>

	{{ Form::text('subject', $news->subject, ['class' => 'span4', 'placeholder' => 'عنوان خبر']) }}
	<div>
		{{ Form::textarea('content', $news->content, ['class' => 'span6 makeTextNice', 'placeholder' => 'عنوان خبر']) }}
	</div>
	
{{ Form::close() }}

@endsection
