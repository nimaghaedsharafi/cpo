@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')
    <h2> {{ $title }} </h2>
	<?php if(Session::has('msg')) { echo Misc::alert(Session::get('msg'), Session::get('state') ); } ?>

@foreach($articles->results as $article)
<div>
	<div class="span8 well">
        <h2>
        	{{ $article->subject }}
        </h2>
        <h4><small>ثبت در تاریخ {{ Misc::niceDateForge($article->created_at, 'j F y') }}
        <small></h4>
        <p>{{ Misc::cutWord($article->content, 230)}}</p>
        <h6><a class="pull-left btn" href="{{ URL::to_route('showarticle', $article->id) }}">ادامه مقاله... </a></h6>
    </div>
</div>
@endforeach
<div class="pagination pagination-centered">
	{{ $articles->links() }}
</div>
@endsection