<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>
	{{ HTML::style('css/bootstrap.min.css', ['media' => 'screen,print']); }}
</head>
<body>
	@yield('content')

@yield('script')
<script type="text/javascript">window.print()</script>
</body>
</html>