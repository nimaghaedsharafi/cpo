@layout('master')

@section('title')
	{{ $title }}
@endsection

@section('content')
	<h2> {{ $title }} <button class="btn btn-primary" id="newuser"> کاربر جدید </button></h2>

<?php if(Session::has('msg')) { echo Misc::alert(Session::get('msg'), Session::get('state') ); } ?>

{{ $table }}

<div class="pagination pagination-centered">
	{{ $pagination }}
</div>
@endsection

@section('script')
<script type="text/javascript">
	$('button#newuser').click(function(e) { ajaxModal(e, 'newusermodal', '{{ URL::to_route('newuser') }}') });
	$('a.show').click(function(e) { newAjaxModal(e, 'showdailyact', $(this).attr('href')) });
</script>
@endsection
