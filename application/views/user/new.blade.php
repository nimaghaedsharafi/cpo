{{ Form::open(URL::to_route('newuser')) }}
<div id="newusermodal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>کاربر جدید</h3>
	</div>

		<div class="modal-body">
		<label for="name">نام</label>
			{{ Form::input('text', 'name', '', ['placeholder' => 'نام', 'class' => 'span4'] ) }}
		<label for="username">نام کاربری</label>
			{{ Form::input('text', 'username', '', ['placeholder' => 'نام کاربری', 'class' => 'span4'] ) }}
		<label for="password">رمز عبور</label>
			{{ Form::input('text', 'password', '', ['placeholder' => 'رمز عبور', 'class' => 'span4'] ) }}
		<label for="access">سطح دسترسی</label>
			{{ Form::select('access', ['0' => 'مدیر', '1' => 'کاربر',], 1, ['class' => 'span4']) }}
	</div>
	<div class="modal-footer">
		<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">لغو</button>
		<input type="submit" class="btn btn-primary" value="ذخیره" />
	</div>
</div>
</form>
</script>