<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>{{ 'ورود به سایت' }}</title>

    <!-- Le styles -->
    {{ HTML::style("css/bootstrap.min.css") }}
	{{ HTML::style('css/style.css')}}
    {{ HTML::style('css/login.css')}}
  </head>

  <body>
    <div class="boxcontainer">
		{{ Form::open(URL::to_route('login'), 'POST' , array('class' => 'form-signin') ); }}
      	{{ Form::token( ); }}
	        <h2 class="form-signin-heading">ورود به سایت <small><h4>امروز {{ Misc::niceDate() }}</h4></small></h2>
	        <?php if(Session::get('msg') != '' ) { ?>
			<div class="alert <?php if(Session::has('state')) { echo 'alert-' . Session::get('state'); } else { echo 'alert-error'; }?> ">
				{{ Session::get('msg') }}
				<a class="close" href="#" data-dismiss="alert" type="button">×</a>
			</div>
			<?php } ?>
	        <input type="text" class="input-block-level" placeholder="نام کاربری" name="username">
	        <input type="password" class="input-block-level" placeholder="رمز عبور" name="password">
	        <label class="checkbox">
	        	<input type="checkbox" name="rememberme" value="1" checked="checked" > مرا به خاطر بسپار
	        	<button id="submit" class="btn btn-large btn-primary" type="submit">ورود</button>
	        </label>
		{{ Form::close() }}

    </div> <!-- /boxcontainer -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    {{ HTML::script("js/jquery.js"); }}
    {{ HTML::script("js/bootstrap.min.js"); }}
    {{ HTML::script('js/ajax.js') }}

    <script type="text/javascript">
		(function() {
			var box = $('div.boxcontainer');

			box.center();
		})();
		$(window).resize(function(event) {
			var box = $('div.boxcontainer');
			box.center();
		});
	</script>
  </body>
</html>
