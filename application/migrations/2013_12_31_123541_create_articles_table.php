<?php

class Create_Articles_Table {    

	public function up()
    {
		Schema::create('articles', function($table) {
			$table->increments('id');
			$table->string('subject');
			$table->text('content');
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('articles');

    }

}