<?php

class Create_Contactus_Table {    

	public function up()
    {
		Schema::create('contactus', function($table) {
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->text('message');
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('contactus');

    }

}