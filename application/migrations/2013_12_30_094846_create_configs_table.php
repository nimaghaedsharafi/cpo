<?php

class Create_Configs_Table {    

	public function up()
    {
		Schema::create('configs', function($table) {
			$table->increments('id');
			$table->string('ckey');
			$table->text('cvalue');
	});

    }    

	public function down()
    {
		Schema::drop('configs');

    }

}