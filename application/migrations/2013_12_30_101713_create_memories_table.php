<?php

class Create_Memories_Table {    

	public function up()
    {
		Schema::create('memories', function($table) {
			$table->increments('id');
			$table->string('subject');
			$table->text('mem');
			$table->integer('uid');
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('memories');

    }

}