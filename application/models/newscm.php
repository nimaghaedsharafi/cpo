<?php

class Newscm extends Eloquent 
{
	public static $table = 'newscms';

	public function news()
	{
		return $this->belongs_to('news', 'nid');
	}
}