<?php

class Report extends Eloquent 
{
	public static $table = 'reports';

	public function author()
	{
		return $this->belongs_to('user', 'uid');
	}
}