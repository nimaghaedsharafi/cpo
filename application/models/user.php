<?php

class User extends Eloquent 
{
	public static $table = 'users';

	public function mem()
	{
		return $this->has_many('mem', 'uid');
	}
	public function report()
	{
		return $this->has_many('report', 'uid');
	}
	public function news()
	{
		return $this->has_many('news', 'uid');
	}

	public function delete()
	{
		$this->news()->delete();
		$this->report()->delete();
		$this->mem()->delete();
		
		return parent::delete();
	}
}