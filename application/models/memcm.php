<?php

class Memcm extends Eloquent 
{
	public static $table = 'memcms';

	public function memory()
	{
		return $this->belongs_to('mem', 'mid');
	}
}