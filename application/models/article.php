<?php

class Article extends Eloquent 
{
	public static $table = 'articles';

	public function author()
	{
		return $this->belongs_to('user', 'uid');
	}
}