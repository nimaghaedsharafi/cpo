<?php

class Home_Controller extends Base_Controller
{
	public function get_index()
	{
		$data['title'] = 'صفحه اصلی';
		$data['news'] = News::with('author')->order_by('id', 'desc')->take(10)->get();

		return View::make('home.index', $data);
	}
	public function get_contactus()
	{
		$data['title'] = 'ارتباط با ما';
		return View::make('home.contactus', $data);
	}
	public function post_contactus()
	{
		$input = Input::all();

		$data = [
			'name' => $input['name'],
			'email' => $input['email'],
			'message' => $input['message']
		];

		Contact::create($data);

		return Redirect::to_route('contactus')->with('msg', 'با تشکر ، پیام شما با موفقیت ثبت شد.')->with('state', 'info');
	}
	public function get_aboutus()
	{
		$aboutus = Cnfg::getConfig('aboutus');

		$data['title'] = 'درباره ی ما';
		$data['content'] = $aboutus->cvalue;
	
		return View::make('home.aboutus', $data);
	}
	public function get_settings()
	{
		$data['title'] = 'تنظیمات';
		$data['name'] = Cnfg::getConfig('portalname')->cvalue;
		$data['aboutus'] = Cnfg::getConfig('aboutus')->cvalue;
		return View::make('home.settings', $data);
	}
	public function post_settings()
	{
		$input = Input::all();

		$portalname = Cnfg::where('ckey', 'like', 'portalname')->first();
		$portalname->cvalue = $input['name'];
		$portalname->save();

		$aboutus = Cnfg::where('ckey', 'like', 'aboutus')->first();
		$aboutus->cvalue = $input['aboutus'];
		$aboutus->save();

		return Redirect::to_route('settings')->with('msg', 'تنظیمات با موفقیت ثبت شد.')->with('state', 'info');
	}
}
