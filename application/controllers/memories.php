<?php

class Memories_Controller extends Base_Controller {    

	public function get_index()
    {
        $data['title'] = 'خاطره ها';
        $data['memories'] = Mem::with('author')->order_by('id', 'desc')->paginate();

        return View::make('memories.index', $data);
    }    

	public function get_create()
    {
        $data['title'] = 'خاطره جدید';
        
        return View::make('memories.new', $data);
    }    

	public function post_create()
    {
        $input = Input::all();

        $data = [
            'subject' => $input['subject'],
            'mem' => $input['mem']
        ];

        Auth::User()->mem()->insert($data);

        return Redirect::to_route('memoriesindex')->with('msg', 'خاطره شما با موفقیت ثبت شد.')->with('state', 'info');
    }    

	public function get_update($id)
    {
        $data['title']  = 'ویرایش خاطره';
        $data['mem'] = Mem::find($id);

        return View::make('memories.edit', $data);
    }    

	public function post_update()
    {
        $input = Input::all();

        $mem = Mem::find($input['mid']);
        $mem->subject = $input['subject'];
        $mem->mem = $input['mem'];
        $mem->save();

        return Redirect::to_route('memoriesindex')->with('msg', 'خاطره شما با موفقیت ویرایش گردید.')->with('state', 'info');
    }    

	public function get_show($id)
    {
        $data['title'] = 'مشاهده خاطره';
        $data['mem'] = Mem::with('author', 'comments')->find($id);

        return View::make('memories.show', $data);
    }    

	public function get_delete($id)
    {
        $mem = Mem::find($id);
        $mem->delete();

        return Redirect::to_route('memoriesindex')->with('msg', 'خاطره شما با موفقیت حذف گردید.')->with('state', 'info');
    }

    public function post_leavecomment()
    {
        $input = Input::all();

        $mem = Mem::find($input['mid']);

        $data = [
            'name' => $input['name'],
            'comment' => $input['comment'],
            'accepted' => 0,
        ]; 
        
        $msg = 'نظر شما با موفقیت ثبت گردید پس از تائید مدیر نظر شما نمایش داده می شود.';
        if(Auth::User()->access == ADMIN_ACCESS)
        {
            $data['accepted'] = 1;
            $msg = 'نظر شما با موفقیت ثبت گردید.';
        }

        $mem->comments()->insert($data);

        return Redirect::to_route('showmemory', $mem->id)->with('msg', $msg)->with('state', 'info');
    }
    public function get_verifycm($id, $flag = 0)
    {
        $mc = Memcm::find($id);

        $mc->accepted = $flag;
        $mc->save();
        
        $msg = 'نظر با موفقیت غیر قابل رویت شد.';
        if($flag)
        {
            $msg = 'نظر با موفقیت قابل رویت شد.';
        }
        return Redirect::to_route('showmemory', $mc->mid)->with('msg', $msg)->with('state', 'info');
    }
    public function get_acceptcm($id)
    {
        $mc = Memcm::find($id);

        $mc->accepted = true;
        $mc->save();
    
        $msg = 'نظر با موفقیت قابل رویت شد.';

        return Redirect::to_route('commentsindex')->with('msg', $msg)->with('state', 'info');
    }
    public function get_commentsindex()
    {
        $data['title'] = 'تائید نظرات';
        $dbData = Memcm::with('memory')->where('accepted', '=', '0')->order_by('id', 'desc')->paginate();
        $table = new MyTable($this->myTableConfig($dbData->results));
        $data['table'] = $table->render();
        $data['pagination'] = $dbData->links();

        return View::make('memories.commentsindex', $data);
    }
    public function myTableConfig($dbData)
    {
        $config['headers'] = [
            'name' => new Header('نام', '', false), 
            'created_at' => new Header('تاریخ ثبت', '', false),
            'comment' => new Header('نظر', '', false), 
            'operation' => new Header('عملیات', '', false),
        ];

        $config['cells'] = [
            new Cell('name', '', ''),
            new Cell('created_at', 'Misc::niceDateForge', 'j F y - H:i'),
            new Cell('comment', '', ''),
            new Cell('operation', '', ['URL::to_route(\'acceptcm\', $row->id)']),
        ];

        $config['operation'] = '<a href="%s" class="btn"><i class="icon-eye-open"></i></a>';
        $config['data'] = $dbData;
        $config['onEmpty'] = 'نظر غیر قابل رویتی موجود نیست.';

        return $config;
    }
}