<?php

class News_Controller extends Base_Controller 
{
	public function get_index()
    {
        $data['title'] = 'اخبار';
        $data['news'] = News::with('author')->order_by('id', 'desc')->paginate();

        return View::make('news.index', $data);
    }    

	public function get_create()
    {
        $data['title'] = 'خبر جدید';
        
        return View::make('news.new', $data);
    }    

	public function post_create()
    {
        $input = Input::all();

        $data = [
            'subject' => $input['subject'],
            'content' => $input['content']
        ];

        Auth::User()->news()->insert($data);

        return Redirect::to_route('newsindex')->with('msg', 'خبر شما با موفقیت ثبت شد.')->with('state', 'info');
    }    

	public function get_update($id)
    {
        $data['title']  = 'ویرایش خبر';
        $data['news'] = News::find($id);

        return View::make('news.edit', $data);
    }    

	public function post_update()
    {
        $input = Input::all();

        $news = News::find($input['nid']);
        $news->subject = $input['subject'];
        $news->content = $input['content'];
        $news->save();

        return Redirect::to_route('newsindex')->with('msg', 'خبر شما با موفقیت ویرایش گردید.')->with('state', 'info');
    }    

	public function get_show($id)
    {
        $data['title'] = 'مشاهده خبر';
        $data['news'] = News::with('author', 'comments')->find($id);

        return View::make('news.show', $data);
    }    

	public function get_delete($id)
    {
        $news = News::find($id);
        $news->delete();

        return Redirect::to_route('newsindex')->with('msg', 'خبر شما با موفقیت حذف گردید.')->with('state', 'info');
    }

    public function post_leavecomment()
    {
        $input = Input::all();

        $news = News::find($input['nid']);

        $data = [
            'name' => $input['name'],
            'comment' => $input['comment'],
            'accepted' => 0,
        ]; 
        
        $msg = 'نظر شما با موفقیت ثبت گردید پس از تائید مدیر نظر شما نمایش داده می شود.';
        if(Auth::User()->access == ADMIN_ACCESS)
        {
            $data['accepted'] = 1;
            $msg = 'نظر شما با موفقیت ثبت گردید.';
        }

        $news->comments()->insert($data);

        return Redirect::to_route('shownews', $news->id)->with('msg', $msg)->with('state', 'info');
    }
    public function get_verifyncm($id, $flag = 0)
    {
        $nc = Newscm::find($id);

        $nc->accepted = $flag;
        $nc->save();
        
        $msg = 'نظر با موفقیت غیر قابل رویت شد.';
        if($flag)
        {
            $msg = 'نظر با موفقیت قابل رویت شد.';
        }
        return Redirect::to_route('shownews', $nc->nid)->with('msg', $msg)->with('state', 'info');
    }
    public function get_acceptcm($id)
    {
        $mc = Newscm::find($id);

        $mc->accepted = true;
        $mc->save();
    
        $msg = 'نظر با موفقیت قابل رویت شد.';

        return Redirect::to_route('newscommentsindex')->with('msg', $msg)->with('state', 'info');
    }
    public function get_commentsindex()
    {
        $data['title'] = 'تائید نظرات';
        $dbData = Newscm::with('news')->where('accepted', '=', '0')->order_by('id', 'desc')->paginate();
        $table = new MyTable($this->myTableConfig($dbData->results));
        $data['table'] = $table->render();
        $data['pagination'] = $dbData->links();

        return View::make('news.commentsindex', $data);
    }
    public function myTableConfig($dbData)
    {
        $config['headers'] = [
            'name' => new Header('نام', '', false), 
            'created_at' => new Header('تاریخ ثبت', '', false),
            'comment' => new Header('نظر', '', false), 
            'operation' => new Header('عملیات', '', false),
        ];

        $config['cells'] = [
            new Cell('name', '', ''),
            new Cell('created_at', 'Misc::niceDateForge', 'j F y - H:i'),
            new Cell('comment', '', ''),
            new Cell('operation', '', ['URL::to_route(\'acceptncm\', $row->id)']),
        ];

        $config['operation'] = '<a href="%s" class="btn"><i class="icon-eye-open"></i></a>';
        $config['data'] = $dbData;
        $config['onEmpty'] = 'نظر غیر قابل رویتی موجود نیست.';

        return $config;
    }
}