<?php

class Users_Controller extends Base_Controller
{

	public function get_index()
    {
        $data['title'] = 'مدیریت کاربران';
        $dbData = User::order_by('id', 'desc')->paginate();
        $table = new MyTable($this->myTableConfig($dbData->results));
        $data['table'] = $table->render();
        $data['pagination'] = $dbData->links();

        return View::make('user.index', $data);
    }    
    public function myTableConfig($dbData)
    {
        $config['headers'] = [
            'name' => new Header('نام', '', false), 
            'username' => new Header('نام کاربری', '', false), 
            'access' => new Header('مدیر', '', false),
            'operation' => new Header('عملیات', '', false),
        ];

        $config['cells'] = [
            new Cell('name', '', ''),
            new Cell('username', '', ''),
            new Cell('access', 'Misc::accessLabel', '$row->access'),
            new Cell('operation', '', ['URL::to_route(\'deluser\', $row->id)']),
        ];

        $config['operation'] = '<div class="btn-group  pull-right">
            <button class="btn">عملیات</button>
            <button class="btn dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
            <li><a href="%s"><i class="icon-trash" ></i> حذف</a></li>
            </ul>
        </div>
        ';
        $config['data'] = $dbData;
        $config['onEmpty'] = 'نظر غیر قابل رویتی موجود نیست.';

        return $config;
    }

	public function get_new()
    {
        return View::make('user.new');
    }    

	public function post_new()
    {
        $input = Input::all();
        $data = [
            'name' => $input['name'],
            'password' => Hash::make($input['password']),
            'username' => $input['username'],
            'access' => $input['access'],
        ];
        User::create($data);

        return Redirect::to_route('usersindex')->with('msg', 'کاربر با موفقیت ثبت شد.')->with('state', 'info');
    }    

	public function get_delete($id)
    {
        $user = User::find($id);

        $user->delete();

        return Redirect::to_route('usersindex')->with('msg', 'کاربر با موفقیت حذف شد.')->with('state', 'info');
    }

}