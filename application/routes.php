<?php
require_once path('app') . 'filters.php';
require_once path('app') . 'errorhandler.php';

Route::get('/', [ 'as' => 'home', 'uses' => 'home@index' ]);
Route::post('login', [ 'before' => 'crsf' , 'as' => 'login', 'uses' => 'auth@login' ]);
Route::get('login', ['as' => 'login', 'uses' => 'auth@login' ]);
Route::get('logout', [ 'before' => 'auth' , 'as' => 'logout', 'uses' => 'auth@logout' ]);
Route::get('contactus', [ 'as' => 'contactus', 'uses' => 'home@contactus' ]);
Route::post('contactus', [ 'before' => 'crsf' , 'as' => 'contactus', 'uses' => 'home@contactus' ]);
Route::get('aboutus', [ 'as' => 'aboutus', 'uses' => 'home@aboutus' ]);

# Memories Routes
Route::get('memories/index', ['as' => 'memoriesindex', 'uses' => 'memories@index']);
Route::get('memories/new', [ 'before' => 'auth', 'as' => 'newmemory', 'uses' => 'memories@create' ]);
Route::post('memories/new', ['as' => 'newmemory', 'uses' => 'memories@create' ]);
Route::get('memories/(:num)/edit', [ 'as' => 'editmemory', 'uses' => 'memories@update' ]);
Route::post('memories/edit', ['as' => 'updatememory', 'uses' => 'memories@update' ]);
Route::get('memories/(:num)/show', [ 'as' => 'showmemory', 'uses' => 'memories@show' ]);
Route::get('memories/(:num)/delete', [ 'as' => 'deletememory', 'uses' => 'memories@delete' ]);
Route::post('memories/leavecomment', [ 'as' => 'leavecomment', 'uses' => 'memories@leavecomment' ]);
Route::get('memories/comments/(:num)/toggle/(:num?)', ['as' => 'verifycm', 'uses' => 'memories@verifycm' ]);
Route::get('memories/comments/index', [ 'as' => 'commentsindex', 'uses' => 'memories@commentsindex' ]);
Route::get('memories/comments/(:num)/accept', ['as' => 'acceptcm', 'uses' => 'memories@acceptcm' ]);

# Reports Routes
Route::get('reports/index', ['before' => 'auth', 'as' => 'reportsindex', 'uses' => 'reports@index']);
Route::get('reports/new', [ 'before' => 'auth', 'as' => 'newreport', 'uses' => 'reports@create' ]);
Route::post('reports/new', ['as' => 'newreport', 'uses' => 'reports@create' ]);
Route::get('reports/(:num)/edit', [ 'before' => 'auth', 'as' => 'editreport', 'uses' => 'reports@update' ]);
Route::post('reports/edit', ['as' => 'updatereport', 'uses' => 'reports@update' ]);
Route::get('reports/(:num)/show', [ 'before' => 'auth', 'as' => 'showreport', 'uses' => 'reports@show' ]);
Route::get('reports/(:num)/delete', [ 'before' => 'auth', 'as' => 'deletereport', 'uses' => 'reports@delete' ]);

# Articles Routes
Route::get('article/index', ['before' => 'auth', 'as' => 'articleindex', 'uses' => 'article@index']);
Route::get('article/new', [ 'before' => 'auth', 'as' => 'newarticle', 'uses' => 'article@create' ]);
Route::post('article/new', ['as' => 'newarticle', 'uses' => 'article@create' ]);
Route::get('article/(:num)/edit', [ 'before' => 'auth', 'as' => 'editarticle', 'uses' => 'article@update' ]);
Route::post('article/edit', ['before' => 'auth', 'as' => 'updatearticle', 'uses' => 'article@update' ]);
Route::get('article/(:num)/show', [ 'before' => 'auth', 'as' => 'showarticle', 'uses' => 'article@show' ]);
Route::get('article/(:num)/delete', [ 'before' => 'auth', 'as' => 'deletearticle', 'uses' => 'article@delete' ]);

# News Routes
Route::get('news/index', ['before' => 'auth', 'as' => 'newsindex', 'uses' => 'news@index']);
Route::get('news/new', [ 'before' => 'auth', 'as' => 'newnews', 'uses' => 'news@create' ]);
Route::post('news/new', ['as' => 'newnews', 'uses' => 'news@create' ]);
Route::get('news/(:num)/edit', [ 'before' => 'auth', 'as' => 'editnews', 'uses' => 'news@update' ]);
Route::post('news/edit', ['as' => 'updatenews', 'uses' => 'news@update' ]);
Route::get('news/(:num)/show', [ 'before' => 'auth', 'as' => 'shownews', 'uses' => 'news@show' ]);
Route::get('news/(:num)/delete', [ 'before' => 'auth', 'as' => 'deletenews', 'uses' => 'news@delete' ]);
Route::post('news/leavencomment', [ 'as' => 'leavencomment', 'uses' => 'news@leavecomment' ]);
Route::get('news/comments/(:num)/toggle/(:num?)', ['as' => 'verifyncm', 'uses' => 'news@verifyncm' ]);
Route::get('news/comments/(:num)/accept', ['as' => 'acceptncm', 'uses' => 'news@acceptcm' ]);
Route::get('news/comments/index', [ 'as' => 'newscommentsindex', 'uses' => 'news@commentsindex' ]);

# User and Settings
Route::get('settings', ['as' => 'settings', 'uses' => 'home@settings' ]);
Route::post('settings', [ 'as' => 'settings', 'uses' => 'home@settings' ]);
Route::get('users/index', ['as' => 'usersindex', 'uses' => 'users@index' ]);
Route::get('user/new', ['as' => 'newuser', 'uses' => 'users@new' ]);
Route::post('user/new', [ 'as' => 'newuser', 'uses' => 'users@new' ]);
Route::get('user/(:num)/delete', ['as' => 'deluser', 'uses' => 'users@delete' ]);


Route::get('install', function(){
	User::create([
		'username' => 'admin',
		'password' => Hash::make('admin'),
		'name' => 'مدیرسایت',
		'access' => ADMIN_ACCESS,
	]);
	return Redirect::to_route('usersindex');
});